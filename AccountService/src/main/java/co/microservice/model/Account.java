package co.microservice.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Account {
    @Id
    private int id;
    private String number;
    private double balance;
    private String cust_id;

    public Account() {
    }

    public Account(int id, String number, double balance, String cust_id) {
        this.id = id;
        this.number = number;
        this.balance = balance;
        this.cust_id = cust_id;
    }

    public int getAccountId() {
        return id;
    }

    public void setAccountId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }
}
