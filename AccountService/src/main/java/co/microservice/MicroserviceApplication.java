package co.microservice;

import co.microservice.model.Account;
import co.microservice.repo.AccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MicroserviceApplication implements CommandLineRunner {
    @Autowired
    AccountRepo accountRepo;

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceApplication.class, args);

    }

    @Override
    public void run(String... args) throws Exception {
        accountRepo.save(new Account(1, "123456", 100.00, "101"));
    }
}
