package co.microservice.controller;

import co.microservice.model.Account;
import co.microservice.repo.AccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class AccountController {
    @Autowired
    private AccountRepo accRepo;

    @GetMapping("/account/{number}")
    public Optional<Account> findByNumber(@PathVariable int number){
        return accRepo.findByNumber(number);
    }

    @GetMapping("/account")
    public List<Account> findAll(){
        return findAll();
    }

    @PostMapping("/addAccount")
    public void addAccount(Account account){
        accRepo.save(account);
    }
}
