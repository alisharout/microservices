package com.insight.microservices.currencyconversionservice.controller;

import com.insight.microservices.currencyconversionservice.feign.CurrencyExchangeServiceProxy;
import com.insight.microservices.currencyconversionservice.model.CurrencyConversionBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RestController
public class CurrencyConvertionController {
    Logger logger= LoggerFactory.getLogger(this.getClass());
    @Autowired
    private CurrencyExchangeServiceProxy proxy;

    @GetMapping("/currency-converter/from/{currencyFrom}/to/{currencyTo}/quantity/{quantity}")
    public CurrencyConversionBean convertCurrency(@PathVariable("currencyFrom") String currencyFrom, @PathVariable("currencyTo") String currencyTo,
                                                  @PathVariable BigDecimal quantity){
        Map<String,String> uriVariables= new HashMap<>();
        uriVariables.put("currencyFrom",currencyFrom);
        uriVariables.put("currencyTo", currencyTo);
       ResponseEntity<CurrencyConversionBean> responseEntity= new RestTemplate().getForEntity("http://localhost:8000/currency-exchange/from/{currencyFrom}/to/{currencyTo}",
                CurrencyConversionBean.class,uriVariables);
       CurrencyConversionBean response= responseEntity.getBody();
       return new CurrencyConversionBean(response.getId(),response.getCurrencyFrom(),response.getCurrencyTo(),response.getConversionMultiple(),
               quantity, quantity.multiply(response.getConversionMultiple()),response.getPort());

    }
    @GetMapping("/currency-converter-feign/from/{currencyFrom}/to/{currencyTo}/quantity/{quantity}")
    public CurrencyConversionBean convertCurrencyFeign(@PathVariable("currencyFrom") String currencyFrom,
                                                       @PathVariable("currencyTo") String currencyTo,
                                                       @PathVariable("quantity") BigDecimal quantity){
        CurrencyConversionBean response= proxy.retrieveExchangeValue(currencyFrom,currencyTo);
        logger.info("{}",response);

        return new CurrencyConversionBean(response.getId(),response.getCurrencyFrom(),response.getCurrencyTo(),response.getConversionMultiple(),
                quantity,quantity.multiply(response.getConversionMultiple()),response.getPort());
    }
}
