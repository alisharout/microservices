package com.insight.microservices.currencyexchangeservice.controller;

import com.insight.microservices.currencyexchangeservice.model.ExchangeValue;

import com.insight.microservices.currencyexchangeservice.repository.ExchangeValueRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class CurrencyExchangeController {
    Logger logger= LoggerFactory.getLogger(this.getClass());
    @Autowired
    private Environment environment;
    @Autowired
    private ExchangeValueRepo repository;
    @GetMapping("/currency-exchange/from/{currencyFrom}/to/{currencyTo}")
    public ExchangeValue retrieveExchangeValue(@PathVariable String currencyFrom, @PathVariable String currencyTo){
        /*return new ExchangeValue(1000L,from,to,
                BigDecimal.valueOf(65),Integer.parseInt(environment.getProperty("local.server.port")));*/
        ExchangeValue exchangeValue= repository.findByCurrencyFromAndCurrencyTo(currencyFrom,currencyTo);
        exchangeValue.setPort(Integer.parseInt(environment.getProperty("local.server.port")));
        logger.info("{}",exchangeValue);
        return exchangeValue;
    }
}
