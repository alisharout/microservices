package com.insight.microservices.currencyexchangeservice.repository;

import com.insight.microservices.currencyexchangeservice.model.ExchangeValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExchangeValueRepo extends JpaRepository<ExchangeValue, Long> {
    ExchangeValue findByCurrencyFromAndCurrencyTo(String currencyFrom, String currencyTo);
}
