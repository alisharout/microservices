package com.insight.microservices.currencyexchangeservice;

import brave.sampler.Sampler;
import com.insight.microservices.currencyexchangeservice.model.ExchangeValue;
import com.insight.microservices.currencyexchangeservice.repository.ExchangeValueRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import java.math.BigDecimal;

@SpringBootApplication
@EnableEurekaClient
public class CurrencyExchangeServiceApplication implements CommandLineRunner{
    @Autowired
    private ExchangeValueRepo exchangeValueRepo;
    @Autowired
    private Environment environment;
    public static void main(String[] args) {
        SpringApplication.run(CurrencyExchangeServiceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
       exchangeValueRepo.save(new ExchangeValue(1001L,"USD","INR", BigDecimal.valueOf(70),
               Integer.parseInt(environment.getProperty("local.server.port"))));
        exchangeValueRepo.save(new ExchangeValue(1002L,"EUR","INR", BigDecimal.valueOf(80),
                Integer.parseInt(environment.getProperty("local.server.port"))));
        exchangeValueRepo.save(new ExchangeValue(1003L,"CAD","INR", BigDecimal.valueOf(50),
                Integer.parseInt(environment.getProperty("local.server.port"))));
    }
    @Bean
    public Sampler defaultSampler(){
        return Sampler.ALWAYS_SAMPLE;
    }
}
