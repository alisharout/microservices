package com.insight.zipkindistributedtracingserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ZipkinDistributedTracingServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZipkinDistributedTracingServerApplication.class, args);
    }

}
