package com.insight.microservices.limitservice.controller;

import com.insight.microservices.limitservice.configuration.Configuration;
import com.insight.microservices.limitservice.model.LimitConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LimitConfigurationController {

    @Autowired
    private Configuration configuration;

    @GetMapping("/limits")
    public LimitConfiguration retrieveLimitsFromConfiguration(){
        return new LimitConfiguration(configuration.getMinimum(),configuration.getMaximum());
    }
}
