package service.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class JobApplication {

    List<Job> jobs = new ArrayList<>() ;

    @GetMapping("/job")
    public List<Job> getJobs(){
        return jobs;
    }

    @GetMapping("/job/{id}")
    public Job getJobById(@PathVariable int id){
        return jobs.stream().filter(j->j.getId()==id).findAny().get();
    }

    public static void main(String[] args) {
        SpringApplication.run(JobApplication.class, args);
    }

}
