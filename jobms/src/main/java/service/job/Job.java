package service.job;

public class Job {
    private Long id;
    private String jobName;
    private double salary;

    public Job() {
    }

    public Job(Long id, String jobName, double salary) {
        this.id = id;
        this.jobName = jobName;
        this.salary = salary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
